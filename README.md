# apkast - APK fAST analysis

A bash script to automatize the analysis of APKs: unzip, decompile, analyze and extract information.

Features:
* Virustotal analysis
* File tree
* Directories size
* File extensions
* Permissions
* Extract HTTP(s) domains
* Extract IPs
* Detect base64 strings
* Hardcoded credentials
* Write/read SQL DB
* Write/read SQLite DB
* Write internal/external storage
* Root detection
* Emails
* Certificate pinning
* Certificate information
* Entropy


Installation
----
Requirements:
* Linux distro 64-bit

Required packages:
* apktool
* unzip
* jq
* bc

Use:
```bash
git clone https://github.com/brn1337/apkast.git apkast
cd apkast
./apkast.sh -i
```


Usage
----
Use apkast_massive.sh to analyze multiple apks as it already implements job parallelization.
```bash
               __              __ 
  ____ _____  / /______ ______/ /_
 / __ `/ __ \/ //_/ __ `/ ___/ __/
/ /_/ / /_/ / ,< / /_/ (__  ) /_  
\__,_/ .___/_/|_|\__,_/____/\__/  
    /_/  

USAGE: apkast.sh -[hvm] [-V KEY] [-d DIR] <apk|dir>

    <apk> :apk file to parse
    <dir> :source directory in massive mode
    -m    :enable massive mode
    -d    :processing dir (def: ./processing)
    -V    :enable virus total key

    -i    :install dependencies and exit
           NB. uses apt-get and requires root
    -v    :print script version
    -h    :display this message
```

Contributes
---
Thanks to [5amu](https://github.com/5amu) for improving the tool.
Thanks to John Cromartie for the [entropy bit](https://gist.github.com/jcromartie/2571452)
