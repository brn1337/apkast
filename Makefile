#!/usr/bin/make -f

PKGNAME := apkast
VERSION := 1.0

LICENSEDIR := /usr/share/licenses
DOCSDIR    := /usr/share/doc
INSTPREFIX ?= /usr

install:
	install -Dm755 ./${PKGNAME}.sh $(INSTPREFIX)/bin/${PKGNAME}

	install -dm755 $(DOCSDIR)/${PKGNAME}
	install -Dm644 ./README.md $(DOCSDIR)/${PKGNAME}/README.md

	install -dm644 $(LICENSEDIR)/${PKGNAME}
	install -Dm644 ./LICENSE $(LICENSEDIR)/${PKGNAME}/LICENSE
