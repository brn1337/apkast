#!/bin/sh
# ~apkast.sh: https://gitlab.com/brn1337/apkast/
# A bash script to automatize the analysis of APKs: unzip, decompile, analyze 
# and extract information.

VERSION="1.0"
VIRUSTOT_API="https://www.virustotal.com/vtapi/v2/file/rescan"

banner()
{
    echo "               __              __ "
    echo "  ____ _____  / /______ ______/ /_"
    echo " / __ \`/ __ \/ //_/ __ \`/ ___/ __/"
    echo "/ /_/ / /_/ / ,< / /_/ (__  ) /_  "
    echo "\__,_/ .___/_/|_|\__,_/____/\__/  "
    echo "    /_/  "
}

usage()
{
    echo
    echo "USAGE: apkast.sh -[hvm] [-V KEY] [-d DIR] <apk|dir>"
    echo
    echo "    <apk> :apk file to parse"
    echo "    <dir> :source directory in massive mode"
    echo "    -m    :enable massive mode"
    echo "    -d    :processing dir (def: ./processing)"
    echo "    -V    :enable virus total key"
    echo
    echo "    -v    :print script version"
    echo "    -h    :display this message"
    echo
}

err()
{
    if [ -t 1 ] ; then
        # Output to terminal
        echo -e "\033[31m[+] $1\033[00m"
    else
        # Output is redirected to file
        date "+[%Y-%m-%d]-(%H:%M:%S) [ERROR]: $1"
    fi; exit 1
}

msg()
{
    if [ -t 1 ] ; then
        # Output to terminal
        echo -e "\033[32m[-] $1\033[00m"
    else
        # Output is redirected to file
        date "+[%Y-%m-%d]-(%H:%M:%S) [MESSAGE]: $1"
    fi
}

# Print general info (filename, md5sum, sha256sum)
printinfo()
{
    echo "File   : $1"
    echo "MD5    : $2"
    echo "SHA256 : $3"
    echo -n "Date   : "; date --rfc-email
    echo
}

virustot_get_link()
{
    curl -s --request POST --url "$VIRUSTOT_API" -d apikey="$1" -d resource="$2" | jq -r '.permalink'
}

# https://gist.github.com/jcromartie/2571452
entropyImpl() 
{
	echo "scale = 2; $(gzip -c ${1} | wc -c) / $(cat ${1} | wc -c)" | bc
}

# Stopping errors if tool is missing
DEPENDENCIES="unzip jq bc jadx"
checktools()
{
    for i in $DEPENDENCIES
    do
        command -v $i >/dev/null || return 1
    done
}

[ -t 1 ] && banner
checktools || err "You'll need those bins in PATH: $DEPENDENCIES"

# Loop through the arguments and parse them
while [ "$#" -gt 0 ]; do case "$1" in
    -d) shift; dirchoice="$1" ;;
    -V) shift; virustotk="$1" ;;
    -m) massive_mode=1 ;;
    -v) echo "Version: $VERSION"; exit 0 ;;
    -h) usage; exit 0 ;;
    *)  apkfile="$1" ;;
esac; shift; done

# Set to default if not specified
_procdir="${dirchoice:-"./processing/${apkfile?target missing <apk>}"}"
zip="${_procdir}/zip"
src="${_procdir}/src"

# Check if processing dir exists
[ -d "$zip" ] && err "$zip already exists, delete it before running the script"
[ -d "$src" ] && err "$src already exists, delete it before running the script"

if [ -n "$massive_mode" ]
then
    script_path=$( test -L "$0" && readlink "$0" || echo "$0" )
    [ ! -d "" ] && err "$apkfile should be a dir in massive mode"
    # Clean filenames to avoid bad stuff
    find "${apkfile}" -maxdepth 1 -name "*.apk" -type f | rename 's/[^A-Za-z0-9.\/_-]//g' 
    files=$(find ${apkfile} -maxdepth 1 -name "*.apk" -type f)
    msg "Executing apkast on $(echo "$files" | wc -l) files"
    for fname in $files; do
        file=$(basename -- ${fname})
        filename=${file%.*}
        msg "    ${filename}"
        mkdir -p "${_procdir}/${filename}"
        if [ -n "$virustotk" ]
        then
            sh "$script_path" -V "$virustotk" -d "${_procdir}/${filename}" ${fname} > "${_procdir}/${filename}/report.txt" &
        else
            sh "$script_path" -d "${_procdir}/${filename}" ${fname} > "${_procdir}/${filename}/report.txt" &
        fi
    done
    wait
    exit 0
fi

# Checks for apkfile
[ ! -f "${apkfile?target missing <apk>}" ] && err "$apkfile is not a regular file"

# Generate hashes
md5=$( md5sum    "$apkfile" | cut -d' ' -f1 )
sh2=$( sha256sum "$apkfile" | cut -d' ' -f1 )

# Create processing folder structure
mkdir -p "$_procdir/src" "$_procdir/zip"

# Delete folder if ctrl+c is pressed
trap "rm -rf $_procdir/src $_procdir/zip; exit 2" INT

# Greetings and general info
printinfo "$apkfile" "$md5" "$sh2"

# Get virustotal analysis
[ -n "$virustotk" ] && link=$(virustot_get_link $virustotk $md5 ) &
# Unzip contents
msg "Unzipping..."
unzip -qq "$apkfile" -d "$zip" &
# Decompile apk
msg "Decompiling..."
jadx --deobf "$apkfile" -d "$src" 
# Wait for jobs to end
wait
src="$src/sources"
msg "Original APK     : $apkfile"
[ -n "$virustotk" ] && msg "Virus Total Link : $link"
msg "Unzipped APK     : $zip"
msg "Decompiled APK   : $apk"

# Extract information
msg "File tree:"
tree ${zip} -d | sed 's/^/    /'

msg "Directories size:"
du -h ${zip} | sed 's/^/    /'

msg "File extensions:"
find ${zip}/ -type f | rev | cut -d"/" -f1 | grep -F . | cut -d"." -f1 | rev | sort | uniq -c

# add color based on category (https://github.com/aosp-mirror/platform_frameworks_base/blob/master/core/res/AndroidManifest.xml)
msg "Permissions:"
grep -EHio 'android:name="android\.(permission|hardware|intent)[a-z0-9\._]+"' ${src}/resources/AndroidManifest.xml | cut -c 23- | rev | cut -c 2- | rev | sort | uniq | sed 's/^/    /'

msg "HTTP domains:"
grep -rHiEo --binary-files=text "http://[a-z0-9@:.-]+\.[a-z]{2,6}" ${src} | sort | uniq | sed 's/^/    /'

msg "HTTPS domains:"
grep -rHiEo --binary-files=text "https://[a-z0-9@:.-]+\.[a-z]{2,6}" ${src} | sort | uniq | sed 's/^/    /'

msg "IPs:"
grep -rHiEo --binary-files=text "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)" ${src} | sort | uniq | sed 's/^/    /'

msg "Strings encoded in base64 containing only printable characters:"
grep -rHiEo --binary-files=text "(\"|')[A-Za-z0-9+/]{40,}==?(\"|')" ${src} | cut -c 2- | rev | cut -c 2- | rev | sort | uniq | base64 --decode | grep -aE "^[[:print:]]+$" | sed 's/^/    /'

msg "Hardcoded credentials:"
grep -rHiE --binary-files=text "[a-z0-9_\-\.]*(password|pwd|passwd|pass[^a-z]|api|developer|access[^a-z]|account|salt|secret)[a-z0-9_\-\.]* = ('|\").+('|\")" ${src} | sed 's/^[[:space:]]*/    /' | sort | uniq &
grep -rHiE --binary-files=text "[a-z0-9]+:[a-z0-9]+@[a-z0-9.]+\.[a-z]{2,6}" ${src} | sed 's/^[[:space:]]*/    /' | sort | uniq &
grep -rHiE --binary-files=text "('|\")AKIA[a-z0-9]{16}('|\")" ${src} | sort | uniq | sed 's/^[[:space:]]*/    /' &
grep -rHiE --binary-files=text "('|\")AIza[a-z0-9\\_\-]{35}('|\")" ${src} | sort | uniq | sed 's/^[[:space:]]*/    /' &
grep -rHiE --binary-files=text "('|\")R_[0-9a-f]{32}('|\")" ${src} | sort | uniq | sed 's/^[[:space:]]*/    /' &
wait

msg "Write/read SQL DB:"
grep -rHiE --binary-files=text "('|\")jdbc:[a-z0-9]" ${src} | sed 's/^[[:space:]]*/    /' | sort | uniq

msg "Write/read SQLite DB:"
grep -rHiE --binary-files=text "getWritableDatabase\(|getReadableDatabase\(" ${src} | sed 's/^[[:space:]]*/    /' | sort | uniq

msg "Write internal/external storage:"
grep -rHiE --binary-files=text "openFileOutput\(|new FileWriter\(|new FileOutputStream\(" ${src} | sed 's/^[[:space:]]*/    /' | sort | uniq

msg "Root detection:"
# Check for su command or equivalent
grep -rHiE --binary-files=text '"/data/local/"|"/data/local/bin/"|"/data/local/bin/su"|"/data/local/su"|"/data/local/xbin/"|"/data/local/xbin/su"|"/magisk/.core/bin/"|"/sbin/"|"/sbin/su"|"/su/bin/"|"/su/bin/su"|"/su/xbin/"|"/system/bin/"|"/system/bin/.ext/"|"/system/bin/failsafe/"|"/system/bin/failsafe/su"|"/system/bin/su"|"/system/sd/xbin/"|"/system/sd/xbin/su"|"/system/usr/we-need-root/"|"/system/xbin/"|"/system/xbin/su"|"su"' ${src} | sed 's/^[[:space:]]*/    /' | sort | uniq &
# Check for Xposed framework
grep -rHiE --binary-files=text '"com\.saurik\.substrate"|"XposedBridge\.jar"|"/system/lib(64)?/libxposed_art\.so"|"/system/lib64/libxposed_|"/system/xposed|"/cache/recovery/xposed|"/system/framework/XposedBridge\.jar"|"/system/bin/app_process(32|64)_xposed"|"/magisk/xposed/system/lib/libart-disassembler.so"|"/magisk/xposed/|"/system/bin/app_process(32|64)_orig"' ${src} | sed 's/^[[:space:]]*/    /' | sort | uniq 
# Check for dev/test/non-release keys
grep -rHiE --binary-files=text '"test-keys"|"release-keys"|"dev-keys"' ${src} | sed 's/^[[:space:]]*/    /' | sort | uniq &
# Check for superuser
grep -rHiE --binary-files=text 'superuser\.apk|"eu\.chainfire\.supersu"|"/system/app/Superuser\.apk"' ${src} | sed 's/^[[:space:]]*/    /' | sort | uniq &
wait

msg "Emails:"
grep -rHiIEo "[a-zA-Z0-9.-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9.-]+" ${src} | sed 's/^[[:space:]]*/    /' | sort | uniq

msg "Certificate pinning:"
grep -rHiE --binary-files=text "certificatePinner|\"X\.?509\"|setCertificateEntry|TrustManagerFactory" ${src} | sed 's/^[[:space:]]*/    /' | sort | uniq

msg "Certificate information:"
openssl pkcs7 -inform DER -in ${zip}/META-INF/*.?SA -noout -print_certs -text | sed 's/^/    /'

msg "Entropy:"
entropyfiles=$( find ${zip} -type f )

min_ent=0
max_ent=0
sum_ent=0
for ef in "$entropyfiles"
do
    temp_ent=$( entropyImpl "$ef" )
    echo "$min_ent <= $temp_ent" | bc -l && min_ent="$temp_ent"
    echo "$max_ent >= $temp_ent" | bc -l && max_ent="$temp_ent"
    sum_ent=$( echo "$sum_ent + $temp_ent" | bc )
    echo "    $ef: $temp_ent"
done

msg "    Minimum: $min_ent"
msg "    Maximum: $max_ent"
msg "    Average: $sum_ent"
